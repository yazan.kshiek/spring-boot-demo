package com.example.demo.projection;


public interface StudentProjection extends IProjection {

    String getName();

    Integer getAge();
    IProjection getCountry();
}
