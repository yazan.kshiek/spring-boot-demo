package com.example.demo.projection;

public interface DefaultUserProjection extends IProjection {

    String getName();

    String getUsername();
}
