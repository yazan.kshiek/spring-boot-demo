package com.example.demo.projection;

public interface CourseProjection extends IProjection {

    String getName();

    Double getCost();
}
