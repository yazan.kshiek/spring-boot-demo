package com.example.demo.projection;

import com.example.demo.entity.User;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

public interface TrackingInfoUserProjection extends IProjection {

    Date getCreatedAt();

    Date getUpdatedAt();

    String getUsername();

    String getUuid();
}
