package com.example.demo.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;

@Builder
@Getter
@AllArgsConstructor
public class RestApiResponse<T>{
	
	private boolean status;
	private T result;

	public static <T> RestApiResponse<T> ok(T result) {
		return RestApiResponse.<T>builder()
							  .status(true)
							  .result(result)
							  .build();
	}
}
