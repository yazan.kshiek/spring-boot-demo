package com.example.demo.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLIntegrityConstraintViolationException;


@ControllerAdvice(annotations = RestController.class)
@Slf4j
@Configuration
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {


    String generalMsg="Please try again";

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity handleGenericException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity(generalMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public <T> ResponseEntity<RestApiResponse<T>> accessDenied(AccessDeniedException ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity(new RestApiResponse<>(false, ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = CustomException.class)
    public ResponseEntity<RestApiResponse<String>> handleCustomException(CustomException ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity(new RestApiResponse<>(false, ex.getMsg()), ex.getStatus());
    }

    @ExceptionHandler(value = {SQLIntegrityConstraintViolationException.class})
    public <T> ResponseEntity<RestApiResponse<T>> handleSQLIntegrityConstraintViolationException(
            SQLIntegrityConstraintViolationException ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity(new RestApiResponse<>(false, ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(ex.getMessage(), ex);

        String errorMsg = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .findFirst()
                .orElse(ex.getMessage());

        errorMsg = errorMsg.replace("{", "").replace("}", "");
        return new ResponseEntity<>(new RestApiResponse<>(false, ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
