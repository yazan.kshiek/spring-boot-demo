package com.example.demo.repository;

import com.example.demo.entity.Course;
import com.example.demo.projection.CourseProjection;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(excerptProjection = CourseProjection.class)
public interface CourseRepository  extends DbRepository<Course> {
}
