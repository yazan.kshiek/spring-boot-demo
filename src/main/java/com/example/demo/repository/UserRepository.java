package com.example.demo.repository;

import com.example.demo.entity.User;
import com.example.demo.projection.DefaultUserProjection;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends DbRepository<User>{
}
