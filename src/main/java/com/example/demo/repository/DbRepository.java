package com.example.demo.repository;

import com.example.demo.entity.DbEntity;
import com.example.demo.projection.IProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface DbRepository<T extends DbEntity> extends JpaRepository<T, Long> {

    Optional<T> findByUuid(String uuid);

    // use dynamic projections
    <D> Optional<D> findByUuid(String uuid, Class<D> type);
    <D> Optional<D> findById(Long id, Class<D> type);
}
