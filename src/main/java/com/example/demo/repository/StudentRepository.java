package com.example.demo.repository;

import com.example.demo.entity.Student;
import com.example.demo.projection.StudentProjection;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends DbRepository<Student> {
}
