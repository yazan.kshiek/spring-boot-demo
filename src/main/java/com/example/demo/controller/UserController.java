package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.exception.ApiMessageCode;
import com.example.demo.exception.CustomException;
import com.example.demo.exception.RestApiResponse;
import com.example.demo.projection.DefaultUserProjection;
import com.example.demo.projection.IProjection;
import com.example.demo.projection.TrackingInfoUserProjection;
import com.example.demo.repository.DbRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(value = "/users")
public class UserController extends DbController<User> {


    @Autowired
    private UserRepository userRepository;

    @Override
    public DbRepository<User> getRepository() {
        return userRepository;
    }

    @Override
    public Class<? extends IProjection> getDefaultProjection() {
        return DefaultUserProjection.class;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/tracking-info/{id}")
    public RestApiResponse<TrackingInfoUserProjection> getTrackingInfo(@PathVariable("id")Long id) throws CustomException {
        Optional<TrackingInfoUserProjection> res = userRepository
                .findById(id, TrackingInfoUserProjection.class);

        if(!res.isPresent())
            throw CustomException
                    .builder()
                    .msg(extractApiMsg(ApiMessageCode.NOT_FOUND))
                    .status(HttpStatus.NOT_FOUND).build();

        return RestApiResponse.ok(res.get());
    }
}
