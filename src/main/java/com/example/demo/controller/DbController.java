package com.example.demo.controller;

import com.example.demo.entity.DbEntity;
import com.example.demo.exception.ApiMessageCode;
import com.example.demo.exception.CustomException;
import com.example.demo.exception.RestApiResponse;
import com.example.demo.projection.IProjection;
import com.example.demo.repository.DbRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public abstract class DbController<T extends DbEntity> extends BaseController {

    @Autowired
    @Qualifier("messageSource")
    private MessageSource msgSrc;

    public abstract DbRepository<T> getRepository();

    public abstract Class<? extends IProjection> getDefaultProjection();

    public MessageSource getMessageSource() {
        return msgSrc;
    }

    @Operation(summary = "Get a entity by uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the item", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/uuid/{id}"}, method = {RequestMethod.GET})
    public RestApiResponse<IProjection> getByUuid(
            @PathVariable("id") String uuid) throws CustomException {

        if(!this.checkPermission("get"))
            throw CustomException.builder().msg(extractApiMsg(ApiMessageCode.INSUFFICIENT_PRIV))
                    .status(HttpStatus.FORBIDDEN).build();

        IProjection entity = this.getRepository().findByUuid(uuid, getDefaultProjection())
                .orElseThrow(() -> CustomException.builder()
                        .msg(extractApiMsg(ApiMessageCode.NOT_FOUND))
                        .status(HttpStatus.NOT_FOUND)
                        .build());

        return RestApiResponse.ok(entity);

    }

    @Operation(summary = "Get a entity by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the item", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/{id}"}, method = {RequestMethod.GET})
    public ResponseEntity<RestApiResponse<T>> getById(@PathVariable("id") Long id) throws CustomException {
        return this.checkPermission("get")
                ? buildResponse(RestApiResponse.ok(this.getRepository().findById(id)
                .orElseThrow(() -> CustomException.builder().msg(extractApiMsg(ApiMessageCode.NOT_FOUND)).status(HttpStatus.NOT_FOUND).build())), HttpStatus.OK)
                : buildResponse(new RestApiResponse<>(false, extractApiMsg(ApiMessageCode.INSUFFICIENT_PRIV)), HttpStatus.FORBIDDEN);
    }

    @Operation(summary = "Create a new entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Entity created", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content)})
    @RequestMapping(method = {RequestMethod.POST})
    @Transactional
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<RestApiResponse<T>> create(@RequestBody T entity) throws CustomException {
        return this.checkPermission("create")
                ? buildResponse(RestApiResponse.ok(createEntity(entity)), HttpStatus.CREATED)
                : buildResponse(new RestApiResponse<>(false, extractApiMsg(ApiMessageCode.INSUFFICIENT_PRIV)), HttpStatus.FORBIDDEN);
    }

    @Operation(summary = "Create a list of new entities")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Entities created", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content)})
    @RequestMapping(value = {"/all"}, method = {RequestMethod.POST})
    @Transactional
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<RestApiResponse<List<T>>> create(@RequestBody List<T> entities) throws CustomException {
        if (!this.checkPermission("create"))
            return buildResponse(new RestApiResponse(false, extractApiMsg(ApiMessageCode.INSUFFICIENT_PRIV)), HttpStatus.FORBIDDEN);


        List<T> createdList = new ArrayList<>();
        for (T entity : entities) {
            if (entity.getId() != null)
                continue;
            createdList.add(createEntity(entity));
        }
        return buildResponse(RestApiResponse.ok(createdList), HttpStatus.CREATED);
    }

    protected T createEntity(@Valid T entity) throws CustomException {
        if (entity.getId() != null)
            throw CustomException.builder().msg(extractApiMsg(ApiMessageCode.INVALID_DATA)).status(HttpStatus.BAD_REQUEST).build();

        entity.setUuid(UUID.randomUUID().toString());
        entity.setId(1L);
        entity.setCreatedAt(new Date());
        entity.setUpdatedAt(new Date());

        return this.getRepository().save(entity);
    }

    @Operation(summary = "Delete entity by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Entity deleted", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/{id}"}, method = {RequestMethod.DELETE})
    @Transactional
    public ResponseEntity<RestApiResponse<Boolean>> delete(@PathVariable("id") T entity) {
        return this.checkPermission("delete")
                ? buildResponse(RestApiResponse.ok(deleteEntity(entity)), HttpStatus.OK)
                : buildResponse(new RestApiResponse(false, extractApiMsg(ApiMessageCode.INSUFFICIENT_PRIV)), HttpStatus.FORBIDDEN);
    }

    @Operation(summary = "Delete list of entities by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Entity deleted", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/all/{ids}"}, method = {RequestMethod.DELETE})
    @Transactional
    public ResponseEntity<RestApiResponse<Boolean>> delete(@PathVariable("ids") List<Long> ids) throws CustomException {
        if (!this.checkPermission("delete"))
            return buildResponse(new RestApiResponse(false, extractApiMsg(ApiMessageCode.INSUFFICIENT_PRIV)), HttpStatus.FORBIDDEN);

        for (Long id : ids) {
            deleteEntity(getRepository().findById(id)
                    .orElseThrow(() -> CustomException.builder().msg(extractApiMsg(ApiMessageCode.NOT_FOUND)).status(HttpStatus.NOT_FOUND).build()));
        }
        return buildResponse(RestApiResponse.ok(true), HttpStatus.OK);
    }

    protected Boolean deleteEntity(T entity) {
        this.getRepository().delete(entity);
        return true;
    }

    @Operation(summary = "Update entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Entity updated", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(method = {RequestMethod.PUT})
    @Transactional
    public ResponseEntity<RestApiResponse<T>> update(@RequestBody T entity) throws IOException, CustomException {
        return !this.checkPermission("update")
                ? buildResponse(new RestApiResponse(false, extractApiMsg(ApiMessageCode.INSUFFICIENT_PRIV)), HttpStatus.FORBIDDEN)
                : buildResponse(RestApiResponse.ok(updateEntity(entity)), HttpStatus.OK);
    }

    @Operation(summary = "Update entities")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Entity updated", content = {
                    @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", description = "Server error", content = @Content),
            @ApiResponse(responseCode = "403", description = "Access denied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content)})
    @RequestMapping(value = {"/all"}, method = {RequestMethod.PUT})
    @Transactional
    public ResponseEntity<RestApiResponse<List<T>>> update(@RequestBody List<T> list) throws IOException, CustomException {
        if (!this.checkPermission("update"))
            buildResponse(new RestApiResponse(false, extractApiMsg(ApiMessageCode.INSUFFICIENT_PRIV)), HttpStatus.FORBIDDEN);

        List<T> updated = new ArrayList<>();
        for (T entity : list) {
            updated.add(updateEntity(entity));
        }
        return buildResponse(RestApiResponse.ok(updated), HttpStatus.OK);
    }

    protected T updateEntity(T entity) throws IOException, CustomException {
        @Valid T updated = mergeEntityWithJsonNode(getJsonNode(entity, true), getRepository());
        return getRepository().save(updated);
    }

    private boolean checkPermission(String get) {
        return true;
    }

    public <T extends DbEntity> T mergeEntityWithJsonNode(JsonNode node, DbRepository<T> repository) throws IOException, CustomException {
        if (node == null)
            return null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

        if (!node.has("id") || node.get("id").isNull() || node.get("id").asText().isEmpty())
            throw CustomException.builder().msg(extractApiMsg(ApiMessageCode.NOT_FOUND)).status(HttpStatus.NOT_FOUND).build();

        T entity = repository.findById(node.get("id").asLong()).orElseThrow(
                () -> CustomException.builder().msg(extractApiMsg(ApiMessageCode.NOT_FOUND)).status(HttpStatus.NOT_FOUND).build());
        return objectMapper.readerForUpdating(entity).readValue(node);
    }
}
