package com.example.demo.controller;

import com.example.demo.entity.Student;
import com.example.demo.projection.IProjection;
import com.example.demo.projection.StudentProjection;
import com.example.demo.repository.DbRepository;
import com.example.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/students")
public class StudentController extends DbController<Student> {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public DbRepository<Student> getRepository() {
        return studentRepository;
    }

    @Override
    public Class<? extends IProjection> getDefaultProjection() {
        return StudentProjection.class;
    }
}
