package com.example.demo.controller;

import com.example.demo.entity.Course;
import com.example.demo.projection.CourseProjection;
import com.example.demo.projection.IProjection;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.DbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/courses")
public class CourseController extends DbController<Course> {

    @Autowired
    CourseRepository courseRepository;

    @Override
    public DbRepository<Course> getRepository() {
        return courseRepository;
    }

    @Override
    public Class<? extends IProjection> getDefaultProjection() {
        return CourseProjection.class;
    }
}
