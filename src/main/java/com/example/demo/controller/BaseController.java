package com.example.demo.controller;

import com.example.demo.exception.ApiMessageCode;
import com.example.demo.exception.RestApiResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;

import static java.util.Locale.ENGLISH;

@Slf4j
public abstract class BaseController {

    public static SimpleDateFormat DD_MM_YYYY = new SimpleDateFormat("dd-MM-yyyy");

    public abstract MessageSource getMessageSource();


    public static JsonNode getJsonNode(Object o, boolean excludeNull) {

        ObjectMapper objectMapper = new ObjectMapper();
        if (excludeNull)
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper.convertValue(o, JsonNode.class);
    }

    public static ResponseEntity buildResponse(RestApiResponse restApiResponse, HttpStatus status){
        return new ResponseEntity(restApiResponse, status);
    }

    public String extractApiMsg(ApiMessageCode code){
        return getMessageSource().getMessage(code.getMessage(), null, ENGLISH);
    }

    public static boolean isJson(String s) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.readTree(s);
            return true;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static <T> T convertJsonStringToObject(String json, Class<T> objectClass)
            throws JsonMappingException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, objectClass);
    }

    public static String getJsonStringFromObject(Object o) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(o);
    }

    public static String replaceStringParams(String msg, Map<String, String> params) {

        Set<String> keys = params.keySet();
        for (String key : keys) {
            msg = msg.replace("@" + key + "@", params.getOrDefault(key, ""));
        }
        return msg;
    }

    protected void createDownloadResponse(HttpServletResponse response, String fileName, InputStream stream) {
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        this.createDownloadResponse(response, fileName, extension, stream);
    }

    protected void createDownloadResponse(HttpServletResponse response, String fileName, String extension, InputStream stream) {
        response.setContentType(this.getMimeType(extension));
        response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try {
            IOUtils.copy(stream, response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            log.error(e.getMessage());
        }

    }

    protected String getMimeType(String extension) {
        if (extension == null)
            return "application/octet-stream";

         switch (extension.toLowerCase()) {
                case "gif":
                    return "image/gif";
                case "pdf":
                    return "application/pdf";
                case "png":
                    return "image/png";
                case "jpeg":
                    return "image/jpeg";
                default:
                    return "application/octet-stream";
            }

    }

}


