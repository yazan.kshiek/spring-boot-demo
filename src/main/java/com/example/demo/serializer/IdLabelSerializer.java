package com.example.demo.serializer;

import com.example.demo.entity.DbEntity;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class IdLabelSerializer extends JsonSerializer<DbEntity> {


    public void serialize(DbEntity e, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (e == null) {
            jsonGenerator.writeNull();
        } else {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", e.getId());
            jsonGenerator.writeStringField("label", e.getLabel());
            jsonGenerator.writeEndObject();
        }
    }
}
