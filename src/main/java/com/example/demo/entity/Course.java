package com.example.demo.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "COURSES")
@EqualsAndHashCode(callSuper = true)
public class Course extends DbEntity{

    @Column(name = "NAME")
    @NotBlank
    private String name;

    @Column(name = "COST")
    @Min(0)
    private Double cost;

}
