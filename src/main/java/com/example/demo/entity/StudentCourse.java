package com.example.demo.entity;

import com.example.demo.serializer.IdLabelSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "STUDENT_COURSE", uniqueConstraints = @UniqueConstraint(columnNames = { "COUNTRY_ID", "COURSE_ID" }))
@EqualsAndHashCode(callSuper = true)
public class StudentCourse extends DbEntity{

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="COUNTRY_ID", referencedColumnName = "ID")
    @JsonSerialize(using = IdLabelSerializer.class)
    @NotNull
    private Student student;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="COURSE_ID", referencedColumnName = "ID")
    @JsonSerialize(using = IdLabelSerializer.class)
    @NotNull
    private Course course;
}
