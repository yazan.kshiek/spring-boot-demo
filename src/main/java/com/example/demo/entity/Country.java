package com.example.demo.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "COURSES")
@EqualsAndHashCode(callSuper = true)
public class Country extends DbEntity{

    @Column(name = "NAME")
    @NotBlank
    private String name;
}
