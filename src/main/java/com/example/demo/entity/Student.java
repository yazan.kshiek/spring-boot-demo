package com.example.demo.entity;

import com.example.demo.serializer.IdLabelSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "STUDENTS")
public class Student extends DbEntity{

    @Column(name = "NAME")
    @NotBlank
    private String name;

    @Column(name = "age")
    @Min(18)
    @Max(30)
    private Integer age;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="COUNTRY_ID")
    @JsonSerialize(using = IdLabelSerializer.class)
    @NotNull
    private Country country;
}
