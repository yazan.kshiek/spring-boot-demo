package com.example.demo.entity;

import com.example.demo.serializer.IdLabelSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public abstract class DbEntity  implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID")
    @JsonSerialize( using = IdLabelSerializer.class )
    private User createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "ID")
    @JsonSerialize( using = IdLabelSerializer.class )
    private User updatedBy;

    @Column(name = "_UUID")
    private String uuid;

    public String getLabel(){
        return this.getClass().getSimpleName().contains("$") ? this.getClass().getSuperclass().getSimpleName()
                : this.getClass().getSimpleName();
    }
}
