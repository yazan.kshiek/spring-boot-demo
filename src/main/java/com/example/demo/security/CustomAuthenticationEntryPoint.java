package com.example.demo.security;

import com.example.demo.controller.BaseController;
import com.example.demo.exception.ApiMessageCode;
import com.example.demo.exception.RestApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

import static java.util.Locale.ENGLISH;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;



@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    @Autowired
    @Qualifier("messageSource")
    private MessageSource msgSrc;


    @Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {

        response.setStatus(UNAUTHORIZED.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(
                BaseController.getJsonStringFromObject(new RestApiResponse<>(false, extractApiMsg(ApiMessageCode.INVALID_REQUESTER))));
	}

    private String extractApiMsg(ApiMessageCode code){
        return msgSrc.getMessage(code.getMessage(), null, ENGLISH);
    }
}
