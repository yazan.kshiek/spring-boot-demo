package com.example.demo.security;


import static org.springframework.http.HttpStatus.FORBIDDEN;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.demo.exception.ApiMessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class CustomAccessDeniedHandler extends AccessDeniedHandlerImpl {

    @Autowired
    @Qualifier("messageSource")
    private MessageSource msgSrc;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException accessDeniedException) {

        throw new AccessDeniedException(msgSrc.getMessage(ApiMessageCode.INSUFFICIENT_PRIV.getMessage(), null, Locale.ENGLISH));
    }
}
