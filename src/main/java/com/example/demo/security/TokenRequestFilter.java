package com.example.demo.security;

import com.example.demo.controller.BaseController;
import com.example.demo.entity.User;
import com.example.demo.exception.ApiMessageCode;
import com.example.demo.exception.CustomException;
import com.example.demo.exception.RestApiResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Locale.ENGLISH;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;


@Slf4j
@Order(1)
@Component
public class TokenRequestFilter extends OncePerRequestFilter {

    @Autowired
    @Qualifier("messageSource")
    private MessageSource msgSrc;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {


        if (SecurityContextHolder.getContext().getAuthentication() == null) {

            User u = new  User();
            u.setName("yazan");
            u.setPassword("yazan");
            u.setUsername("yazan");
            u.setId(1L);

            UsernamePasswordAuthenticationToken authUser = new UsernamePasswordAuthenticationToken(
                    u, null, u.getAuthorities());


            SecurityContextHolder.getContext().setAuthentication(authUser);

        }

        if(isTokenExpire(request, response)) {

            response.setStatus(UNAUTHORIZED.value());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.getWriter().write(
                    BaseController.getJsonStringFromObject(new RestApiResponse<>(false, extractApiMsg(ApiMessageCode.INVALID_REQUESTER))));
        }

        chain.doFilter(request, response);
    }

    private boolean isTokenExpire(HttpServletRequest request, HttpServletResponse response) {
        // business here





        return false;
    }

    private String extractApiMsg(ApiMessageCode code){
        return msgSrc.getMessage(code.getMessage(), null, ENGLISH);
    }
}
